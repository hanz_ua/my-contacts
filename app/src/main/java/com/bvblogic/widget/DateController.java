package com.bvblogic.widget;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HANZ on 12.07.2015.
 */
public class DateController {

    /**
     * get Time for View
     *
     * @return string of a time HH:mm
     */
    public static String getCurrentTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
    /**
     * get Date for View
     *
     * @return string of a time dd.MM.yyyy
     */
    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getCurrentMonth() {
        DateFormat dateFormat = new SimpleDateFormat("MMM");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
