package com.bvblogic.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * Created by hanz on 17.02.2016.
 */
public class ControllerDraw {

    public Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);
        return mutableBitmap;
    }

    public Bitmap drawCircleInImage(Context context, int selectButton) {
        Drawable myDrawable = context.getResources().getDrawable(R.drawable.circle);
        Bitmap bitmap = convertToBitmap(myDrawable, 500, 500);
        bitmap = bitmap.copy(bitmap.getConfig(), true);     //lets bmp to be mutable
        Canvas canvas = new Canvas(bitmap);                 //draw a canvas in defined bmp

        Paint paint = new Paint();                          //define paint and paint color
        paint.setColor(Color.parseColor("#b1b1b4"));
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);                           //smooth edges

        canvas.drawCircle(bitmap.getHeight() / 2, bitmap.getWidth() / 2, 60, paint);
        drawTextFamilyText(canvas, bitmap.getHeight() - 45, bitmap.getHeight() - 90);
        drawTextFriendText(canvas, bitmap.getHeight() - 150, bitmap.getHeight() - 45);
        drawTextMountainbikeText(canvas, bitmap.getHeight() - 140, bitmap.getHeight() - 20);
        drawTextWorkText(canvas, bitmap.getHeight() - 120, bitmap.getHeight() + 10);

        return bitmap;

    }

    private void drawTextFamilyText(Canvas canvas, int h, int w) {
        Path path = new Path();
        canvas.rotate(-103, h / 2, w / 2);
        path.addCircle(h / 2, h / 2, 180, Path.Direction.CW);
        canvas.drawTextOnPath("Family", path, 0, 10, Resource.position == 1 ? getPainToText(0) : getPainToText(1));
    }

    private void drawTextWorkText(Canvas canvas, int h, int w) {
        Path path = new Path();
        canvas.rotate(-75, h / 2, w / 2);
        path.addCircle(h / 2, w / 2, 250, Path.Direction.CW);
        canvas.drawTextOnPath("Work", path, 20, 20, Resource.position == 2 ? getPainToText(0) : getPainToText(1));
    }

    private void drawTextMountainbikeText(Canvas canvas, int h, int w) {
        Path path = new Path();
        canvas.rotate(265, h / 2, w / 2);
        path.addCircle(h / 2, w / 2, 250, Path.Direction.CW);
        canvas.drawTextOnPath("#Mountainbike", path, 20, 20, Resource.position == 3 ? getPainToText(0) : getPainToText(1));
    }

    private void drawTextFriendText(Canvas canvas, int h, int w) {
        Path path = new Path();
        canvas.rotate(260, h / 2, w / 2);
        path.addCircle(h / 2, w / 2, 250
                , Path.Direction.CW);
        canvas.drawTextOnPath("Best Friends", path, 20, 20, Resource.position == 4 ? getPainToText(0) : getPainToText(1));
    }

    private Paint getPainToText(int color) {
        Paint paint = new Paint();
        if (color == 1)
            paint.setColor(Color.parseColor("#8b8b8f"));
        else
            paint.setColor(Color.WHITE);
        paint.setTextSize(28);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        return paint;
    }
}
