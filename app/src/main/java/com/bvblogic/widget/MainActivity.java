package com.bvblogic.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppWidgetProvider {
    private RemoteViews views;
    Handler handler;
    Context context;


    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.context = context;
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
        for (int i = 0; i < appWidgetIds.length; i++) {
            int currentWidgetId = appWidgetIds[i];
//            String url = "http://www.bvblogic.com";
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.setData(Uri.parse(url));
//            PendingIntent pending = PendingIntent.getActivity(context, 0, intent, 0);
//            views.setOnClickPendingIntent(R.id.button, pending);
            views = new RemoteViews(context.getPackageName(), R.layout.activity_main);


            Intent active;
            PendingIntent actionPendingIntent;
            ArrayList<Integer> btnIds = new ArrayList<>();
            btnIds.addAll(Resource.CLICK_INTENT_ACTIONS_MAP.values());
            for (int j = 0; j < Resource.CLICK_INTENT_ACTIONS_MAP.size(); j++) {
                active = new Intent(context, MainActivity.class);
                active.setAction(Resource.ACTIONS.get(j));
                actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);
                views.setOnClickPendingIntent(btnIds.get(j), actionPendingIntent);
            }
            views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
            Log.d("pos", Resource.position + "   ");
//            views.setImageViewBitmap(R.id.imageView, new ControllerDraw().drawText(context, 0));
            appWidgetManager.updateAppWidget(currentWidgetId, views);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        this.context = context;
        views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
        if (Resource.ACTIONS.contains(action)) {
            int btnId = Resource.CLICK_INTENT_ACTIONS_MAP.get(action);
            switch (btnId) {
//                case R.id.button:
//                    views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
//                    views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context));
//                    ComponentName thisWidget = new ComponentName(context, MainActivity.class);
//                    AppWidgetManager.getInstance(context).updateAppWidget(thisWidget, views);
//                    break;
                case R.id.circleButton:
                    Toast.makeText(context, "circleButton", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.watsappButton:
                    Toast.makeText(context, "whatsappButton", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.facebookButton:
                    Toast.makeText(context, "facebookButton", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.phoneButton:
                    Toast.makeText(context, "phoneButton", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.chatButton:
                    Toast.makeText(context, "chatButton", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople1:
                    Toast.makeText(context, "Aaliyah", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople2:
                    Toast.makeText(context, "Carmen", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople3:
                    Toast.makeText(context, "Robert", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople4:
                    Toast.makeText(context, "Caroline", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople5:
                    Toast.makeText(context, "Markus", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople6:
                    Toast.makeText(context, "Taylor", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imagePeople7:
                    Toast.makeText(context, "Vanessa", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.family:
                    Toast.makeText(context, "family", Toast.LENGTH_SHORT).show();
                    Resource.position = 1;
                    views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
                    break;
                case R.id.work:
                    Toast.makeText(context, "work", Toast.LENGTH_SHORT).show();
                    Resource.position = 2;
                    views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
                    break;
                case R.id.friend:
                    Toast.makeText(context, "friend", Toast.LENGTH_SHORT).show();
                    Resource.position = 4;
                    views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
                    break;
                case R.id.mountainbike:
                    Toast.makeText(context, "mountainbike", Toast.LENGTH_SHORT).show();
                    Resource.position = 3;
                    views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
                    break;
            }
            update(views);

        }
        super.onReceive(context, intent);
    }

    private void update(RemoteViews view) {
        ComponentName thisWidget = new ComponentName(context, MainActivity.class);
        AppWidgetManager.getInstance(context).updateAppWidget(thisWidget, view);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(runnable, 1000);
            views = new RemoteViews(context.getPackageName(), R.layout.activity_main);
            views.setTextViewText(R.id.textView, " " + DateController.getCurrentTime() + "  ");
            views.setTextViewText(R.id.textView2, DateController.getCurrentMonth().split("\\.")[0]);
            views.setTextViewText(R.id.textView3, DateController.getCurrentDate() + " ");
            //views.setImageViewBitmap(R.id.image, new ControllerDraw().drawCircleInImage(context, Resource.position));
            update(views);
        }
    };

}