package com.bvblogic.widget;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hanz on 18.02.2016.
 */
public class Resource {
    static int position = 1;
    //public static final String ACTION_WIDGET_UPDATE = "update";
    public static final String ACTION_WIDGET_CIRCLE = "circle";
    public static final String ACTION_WIDGET_WATSAPP = "watsapp";
    public static final String ACTION_WIDGET_PHONE = "phone";
    public static final String ACTION_WIDGET_FACEBOOK = "facebook";
    public static final String ACTION_WIDGET_CHAT = "chat";
    public static final String ACTION_WIDGET_PEOPLE1 = "people1";
    public static final String ACTION_WIDGET_PEOPLE2 = "people2";
    public static final String ACTION_WIDGET_PEOPLE3 = "people3";
    public static final String ACTION_WIDGET_PEOPLE4 = "people4";
    public static final String ACTION_WIDGET_PEOPLE5 = "people5";
    public static final String ACTION_WIDGET_PEOPLE6 = "people6";
    public static final String ACTION_WIDGET_PEOPLE7 = "people7";
    public static final String ACTION_WIDGET_FAMILY = "family";
    public static final String ACTION_WIDGET_WORK = "work";
    public static final String ACTION_WIDGET_MOUNTAINBIKE = "mountainbike";
    public static final String ACTION_WIDGET_FRIEND = "friend";
    // <Action, ButtonId>
    public static final HashMap<String, Integer> CLICK_INTENT_ACTIONS_MAP = new HashMap<String, Integer>();
    // <Actions>
    public static final ArrayList<String> ACTIONS = new ArrayList<String>();

    static {
        //CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_UPDATE, R.id.button);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_CIRCLE, R.id.circleButton);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_WATSAPP, R.id.watsappButton);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PHONE, R.id.phoneButton);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_FACEBOOK, R.id.facebookButton);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_CHAT, R.id.chatButton);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE1, R.id.imagePeople1);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE2, R.id.imagePeople2);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE3, R.id.imagePeople3);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE4, R.id.imagePeople4);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE5, R.id.imagePeople5);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE6, R.id.imagePeople6);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_PEOPLE7, R.id.imagePeople7);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_FAMILY, R.id.family);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_WORK, R.id.work);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_MOUNTAINBIKE, R.id.mountainbike);
        CLICK_INTENT_ACTIONS_MAP.put(ACTION_WIDGET_FRIEND, R.id.friend);
        // Fill action list
        ACTIONS.addAll(CLICK_INTENT_ACTIONS_MAP.keySet());
    }
}
